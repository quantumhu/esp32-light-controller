# byte 1 - 0x31 
# byte 2 - red value in hex
# byte 3 - green value in hex
# byte 4 - blue value in hex
# byte 5 - range of 0x00 to 0xff of warm white, set rgb to 0 
# byte 6 - range of 0x00 to 0xff of cool white, set rgb to 0
# byte 7 - 0xf0 in color mode, 0x0f in CCT mode
# byte 8 - 0x0f
# byte 9 - checksum, modulo 256 of all the previous bytes added up

# 71 23 0f a3 to turn device on
# 71 24 0f a4 to turn device off

import sys
import socket
import enum

# LOG LEVEL
class LOGGING(enum.Enum):
    INFO = 1
    WARN = 2
    ERR  = 3

# VARIABLES
IP_ADDR = "192.168.137.33"
PORT = 5577
LOG_LEVEL = 0

def calc_checksum(arr):
    checksum = sum(arr) % 256
    arr.append(checksum)
    
def dump_hex(arr):
    og = arr.hex()
    new_str = '0x'
    for i in range(0, len(og)):
        new_str += str(og[i])
        if (i % 2 == 1):
            new_str += " "
            
    return new_str
    
def calc_brightness(r, g, b):
    r2 = r / 255
    g2 = g / 255
    b2 = b / 255
    
    val = max(r2, g2, b2)
    
    return round(val * 100)
    
def query_state():

    cmd = bytearray([0x81, 0x8a, 0x8b])
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    try:
        s.connect((IP_ADDR, PORT))
        
        send_cmd(cmd)
        
        s.settimeout(3)
        response = s.recv(14)
        
        if (LOG_LEVEL >= LOGGING.INFO.value):
            print("response: " + dump_hex(response))
            
        parse_status(response)
            
    except socket.timeout:
        print("timeout: could not get query")
        
    except Exception as e:
        print(e)
        
    finally:
        s.close()
        
def parse_status(response):
    
    # byte 1: 0x81
    # byte 2: 0x35
    # byte 3: on (0x23), off (0x24)
    # byte 4: function type, 0x61 is manual colours
    # byte 5: UNKNOWN
    # byte 6: UNKNOWN
    # byte 7: red
    # byte 8: green
    # byte 9: blue
    # byte 10: warm white value
    # byte 11: UNKNOWN
    # byte 12: cold white value
    # byte 13: color mode (0xf0 for colors, 0x0f for CCT)
    # byte 14: checksum
    
    print("=========== STATUS ===========")
    print("IP:         " + IP_ADDR + ":" + str(PORT))
    print("POWER:      ", end="")
    
    if (response[2] == 0x23):
        print("ON")
    else:
        print("OFF")
        
    print()
        
    print("RED:        " + str(response[6]))
    print("GREEN:      " + str(response[7]))
    print("BLUE:       " + str(response[8]))
    print()
    
    print("BRIGHT LVL: " + str(calc_brightness(response[6], response[7], response[8])) + "%")
    print()
    
    print("WARM WHITE: " + str(response[9]))
    print("COOL WHITE: " + str(response[11]))
    print("==============================")
    
def send_cmd(cmd):

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        s.connect((IP_ADDR, PORT))
        calc_checksum(cmd)
        
        if (LOG_LEVEL >= LOGGING.INFO.value):
            print("sending " + dump_hex(cmd))
            
        s.send(cmd)
        
        if (LOG_LEVEL >= LOGGING.INFO.value):
            print("sent cmd!")

    except Exception as e:
        print(e)
        
    finally:
        s.close()

    
if __name__ == "__main__":
    default_test = bytearray([0x31, 0x00, 0x00, 0xff, 0x00, 0x00, 0xf0, 0x0f])
    
    if (len(sys.argv) == 1):
        send_cmd(default_test)
        
    elif (len(sys.argv) == 2):
        if (sys.argv[1] == "on"):
            on_cmd = bytearray([0x71, 0x23, 0x0f])
            send_cmd(on_cmd)
        elif (sys.argv[1] == "off"):
            off_cmd = bytearray([0x71, 0x24, 0x0f])
            send_cmd(off_cmd)
        elif (sys.argv[1] == "status"):
            query_state()
        else:
            print("ERROR: unrecognized command")
            
    else:
        cmd = bytearray([0x31])
    
        cmd.append(int(sys.argv[1]))
        cmd.append(int(sys.argv[2]))
        cmd.append(int(sys.argv[3]))
        
        cmd.append(0x00)
        cmd.append(0x00)
        cmd.append(0xf0)
        cmd.append(0x0f)
        
        calc_checksum(cmd)
        send_cmd(cmd)
