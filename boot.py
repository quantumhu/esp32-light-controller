import socket
import network
import gc
import esp

ap_ssid = "<Access point setup by ESP32>"
ap_password = "thepassword"

sta_ssid = "<WiFi network that has internet connection>"
sta_password = "thepassword"

esp.osdebug(None)

# automatic gc collection
gc.collect()

# access point setup for the light bulb
ap = network.WLAN(network.AP_IF)
ap.active(True)
ap.config(essid=ap_ssid, authmode=network.AUTH_WPA_WPA2_PSK, password=ap_password)

print("\n\nAP up! IP Info: " + str(ap.ifconfig()[0]))

# wifi connection for web server
wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect(sta_ssid, sta_password)

while wlan.isconnected() == False:
    pass

print("WiFi connected! IP Info: " + str(wlan.ifconfig()[0]))