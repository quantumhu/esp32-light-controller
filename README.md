## ESP32 Light Controller

I got a set of 4 Illume RGB smart bulbs on clearance so I decided to try reverse engineering it. The smart bulb sends commands to a central server, which sends a command back to my local network. I wanted to communicate with the bulb through the local network only.

I used Wireshark to inspect the packets sent between the app [(Illume RGBW)](https://play.google.com/store/apps/details?id=com.illume.wifi) and the light bulb. The packets were unencrypted so that's what made this project possible. The chip inside the light bulb is actually an ESP8266, the WiFi-only version of the ESP32.

Originally the ESP32 uses a C based firmware called Espruino. There's a Python alternative called MicroPython, which I've used for this project. Python makes sending the right packets very easy.

The ESP32 sets up its own access point, as well as connecting to the local WiFi network (station mode). The access point is for the light bulb to connect to, while the station connection is for the web server that the ESP32 serves, so we can control the light bulb.

The html file is used as a base, with sections of it of it being replaced with the right information before it is served to the browser.