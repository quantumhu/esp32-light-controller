import webrepl
import socket
import urequests
import json
#import ssd1306
#from machine import Pin, I2C
# was initially trying to get a small OLED screen to work but the screen didn't work

LOG_LEVEL = 1

def calc_checksum(arr):
    checksum = sum(arr) % 256
    arr.append(checksum)
    
def calc_brightness(r, g, b):
    r2 = r / 255
    g2 = g / 255
    b2 = b / 255
    
    val = max(r2, g2, b2)
    
    return round(val * 100)
    
def query_state():

    cmd = bytearray([0x81, 0x8a, 0x8b])
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    try:
        s.connect((IP_ADDR, PORT))
        
        send_cmd(cmd)
        
        s.settimeout(3)
        response = s.recv(14)
        
        if (LOG_LEVEL >= 1):
            print("response: " + str(response))
            
        return_val = parse_status(response)
            
    except socket.timeout:
        print("timeout: could not get query")
        
    except Exception as e:
        print(e)
        
    finally:
        s.close()

    return return_val
        
def parse_status(response):
    
    # byte 1: 0x81
    # byte 2: 0x35
    # byte 3: on (0x23), off (0x24)
    # byte 4: function type, 0x61 is manual colours
    # byte 5: UNKNOWN
    # byte 6: UNKNOWN
    # byte 7: red
    # byte 8: green
    # byte 9: blue
    # byte 10: warm white value
    # byte 11: UNKNOWN
    # byte 12: cold white value
    # byte 13: color mode (0xf0 for colors, 0x0f for CCT)
    # byte 14: checksum

    code = "setBulb(\"" + str(IP_ADDR) + "\");\n"
    
    if (response[2] == 0x23):
        code += "setState(1);\n"
    else:
        code += "setState(0);\n"
    
    red = response[6]
    green = response[7]
    blue = response[8]
    
    brightness = calc_brightness(red, green, blue)
    code += "setBrightness(" + str(brightness) + ");\n"
    
    red = "{:X}".format(red)
    green = "{:X}".format(green)
    blue = "{:X}".format(blue)

    if (red == "0"):
        red = "00"
    elif (green == "0"):
        green = "00"
    elif (blue == "0"):
        blue = "00"
    code += "setColour(\"" + str(red) + str(green) + str(blue) + "\");\n"
    
    ww = int(response[9])
    cw = int(response[11])
    
    if (ww == 0 and cw == 0):
        code += "setOtherOptions(\"none\");\n"
    elif (ww > 0):
        code += "setOtherOptions(\"ww\");\n"
    elif (cw > 0):
        code += "setOtherOptions(\"cw\");\n"
    else:
        code += "setOtherOptions(\"none\");\n"

    return code
    
def send_cmd(cmd):

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        s.connect((IP_ADDR, PORT))
        calc_checksum(cmd)
        
        if (LOG_LEVEL >= 1):
            print("sending " + str(cmd))
            
        s.send(cmd)
        
        if (LOG_LEVEL >= 1):
            print("sent cmd!")

    except Exception as e:
        print(e)
        
    finally:
        s.close()

print("\n\nWELCOME TO MAIN.py LAND!")

#i2c = I2C(-1, scl=Pin(22), sda=Pin(21))

#oled_width = 128
#oled_height = 64
#oled = ssd1306.SSD1306_I2C(oled_width, oled_height, i2c)

#oled.text("Hello world 1!", 0, 0)
#oled.text("Hello world 2!", 0, 10)
#oled.text("Hello world 3!", 0, 20)
#oled.show()

html = ""

# get available bulbs from an external json source
resp = urequests.get("http://<something>/bulbs.json")
bulbs = json.loads(resp.text)
print("\nList of bulbs from json: " + str(bulbs["bulbs"]) + "\n")

# set default IP as first (maybe only) bulb
IP_ADDR = bulbs["bulbs"][0]
PORT = 5577

# website
def webcode_setup():
    global html

    with open("main.html", "r") as html_code:
        for line in html_code:
            html += line

def website():
    return html

# start uploader
# webrepl is a web tool for MicroPython to upload the python files to the ESP32
webrepl.start()

# website setup
webcode_setup()

# start webserver
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

while True:

    conn, addr = s.accept()
    print("Got a connection from " + str(addr))
    request = conn.recv(1024)

    print("Content = " + str(request))
    request = str(request)

    response = website()

    bulb_list = ""
    for bulb in bulbs["bulbs"]:
        bulb_list += "<option value=\"" + str(bulb) + "\">" + str(bulb) + "</option>"
    response = response.replace("<!--PosA-->", bulb_list)

    if (request.find("GET / HTTP") != -1):
        # first run, get status
        code = query_state()
        response = response.replace("<!--PosB-->", code)

    elif (request.find("GET /?bulb=") != -1):
        # do the change of the light

        cmd = bytearray([0x31])

        content = request.split("HTTP/1.1")
        #print(content)

        query = content[0][6:].strip()
        print(query)

        queries = query.split("&")
        for i in range(len(queries)):
            queries[i] = queries[i].split("=")
        print(queries)

        cont_processing = True

        # we can't tell if state has changed so turn on or off first
        if (queries[1][0] == "state"):
            if (queries[1][1] == "1"):
                on_cmd = bytearray([0x71, 0x23, 0x0f])
                send_cmd(on_cmd)

            elif (queries[1][1] == "0"):
                off_cmd = bytearray([0x71, 0x24, 0x0f])
                send_cmd(off_cmd)
                cont_processing = False

        if (cont_processing):
            if (queries[4][0] == "options"):
                if (queries[4][1] == "ww"):
                    ww_cmd = bytearray([0x31, 0x00, 0x00, 0x00, 0xff, 0x00, 0x0f, 0x0f])
                    send_cmd(ww_cmd)
                    cont_processing = False
                elif (queries[4][1] == "cw"):
                    cw_cmd = bytearray([0x31, 0x00, 0x00, 0x00, 0x00, 0xff, 0x0f, 0x0f])
                    send_cmd(cw_cmd)
                    cont_processing = False

        if (cont_processing):
            # if you get here it means it's colour mode

            # brightness as a decimal
            brightness = int(queries[2][1]) / 100

            red = int(queries[3][1][0:2], 16)
            green = int(queries[3][1][2:4], 16)
            blue = int(queries[3][1][4:6], 16)

            cmd.append(round(red * brightness))
            cmd.append(round(green * brightness))
            cmd.append(round(blue * brightness))

            cmd.append(0x00)
            cmd.append(0x00)
            cmd.append(0xf0)
            cmd.append(0x0f)

            send_cmd(cmd)

    elif (request.find("GET /favicon.ico") == -1):
        response = "<html><head><title>ff</title></head><body>uh oh</body></html>"

    conn.send("HTTP/1.1 200 OK")
    conn.send("Content-Type: text/html; encoding=utf8\nContent-Length: ")
    conn.send(str(len(response)))
    conn.send("\nConnection: close\n")
    conn.send("\n")
    conn.send(response)
    conn.close()

# http://micropython.org/webrepl/#192.168.2.117:8266
# https://randomnerdtutorials.com/micropython-oled-display-esp32-esp8266/